terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
  required_version = ">= 0.13"
}

provider "scaleway" {
  access_key = var.access_key
  secret_key = var.secret_key
  zone       = var.zone
  project_id = var.project_id
}

module "instances" {
  source        = "./modules/sw-instance"
  for_each      = toset(["grafana", "influxdb"])
  instance_name = each.key
  sg_id         = module.sg.sg_id
  depends_on = [
    module.sg
  ]
}

module "sg" {
  source    = "./modules/sw-sg"
  manage_ip = var.manage_ip
  allowed_tcp_port = var.allowed_tcp_port
}