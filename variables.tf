/****
Authentication
 ****/

variable "access_key" {
  description = "Your access key"
  sensitive   = true
}

variable "secret_key" {
  description = "Your secret key"
  sensitive   = true
}

variable "zone" {
  description = "Your zone of your infrastructure"
  default     = "fr-par-1"
}

/****
Project Identication
****/

variable "project_id" {
  description = "Your project ID"
  sensitive   = true
}

variable "manage_ip" {
  description = " IPv4/CIDR of Administrator"
  sensitive   = true
}

/****
Instance
****/

variable "grafana-name" {
  description = "The name of the Instance where Grafana will be install"
  default     = "grafana"
}

variable "influxdb-name" {
  description = "The name of the instance where InfluxDB will be install"
  default     = "influxdb"
}

variable "instance_type" {
  description = "The type of your Instance"
  default     = "PLAY2-NANO"
}

variable "instance_image" {
  description = "The image of your instance"
  default     = "ubuntu_bionic"
}

/****
Security group
****/

variable "inboud_default_policy" {
  description = "The default policy of the inboud of your SG"
  default     = "drop"
}

variable "allowed_tcp_port" {}